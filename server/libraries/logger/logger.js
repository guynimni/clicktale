class logger {
  static log(text) {
    console.log(text); // eslint-disable-line
  }
  static error(text) {
    console.error(text); // eslint-disable-line
  }
}

module.exports = logger;