const path = require('path');
const baseHelper = require('../helpers/baseHelper');
const router = require('express').Router();

router.get('/favicon.ico', function (request, response) {
  response.sendFile(path.resolve(__dirname, '..', '..', baseHelper.getBaseDirectory(), 'favicon.ico'));
});

router.get('*', function (request, response) {
  response.sendFile(path.resolve(__dirname, '..', '..', baseHelper.getBaseDirectory(), 'index.html'));
});

module.exports = router;