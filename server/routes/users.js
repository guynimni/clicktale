const router = require('express').Router();
const UsersDAL = require('../DAL/users');

router.get('/', function (request, response) {
  const { query, page, size } = request.query;

  const data = UsersDAL.get(query, page, size);

  data.results = data.results.map(user => user.toClientInfo());

  response.json(data);
});

module.exports = router;