const app = require('express')();
const setup = require('./setup');
const logger = require('logger');

const PORT = process.env.PORT || 3000;

logger.log("Serving " + (process.env.NODE_ENV || "development"));


setup.registerUsage(app);
setup.registerRoutes(app);
app.listen(PORT, function () {
  logger.log("listening to port " + PORT);
});