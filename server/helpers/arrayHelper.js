module.exports.paginate = function (array, page_size, page_number) {
  --page_number; // page 1 == position 0
  const totalPages = Math.ceil(array.length/page_size);

  return {
    results: array.slice(page_number * page_size, (page_number + 1) * page_size),
    totalPages,
    isEnd: totalPages <= (page_number + 1)
  }
};