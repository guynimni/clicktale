/* eslint-disable no-console */

class baseHelper {

  static getBaseDirectory() {
    if (process.env.NODE_ENV !== 'production')
      return 'client';
    return 'dist';
  }
}

module.exports = baseHelper;