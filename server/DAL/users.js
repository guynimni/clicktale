const User = require('../entities/User');
const usersData = require('../data/users').map(userData => new User(userData));
const arrayHelpers = require('../helpers/arrayHelper');

class UsersDB {
  static get(query = "", page = 1, size = 10) {

    let users = usersData;

    if (query)
      users = usersData.filter(user => user.firstName.includes(query) || user.lastName.includes(query));

    return arrayHelpers.paginate(users, size, page);
  }
}

module.exports = UsersDB;