const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const webpack = require('webpack');
const baseHelper = require('./helpers/baseHelper');
const config = require ('../webpack.config.js');
const logger = require('logger');

//Routes
const usersRouter = require('./routes/users');
const baseRouter = require('./routes/base');

const registerRoutes = function(app) {
  app.use('/users', usersRouter);
  app.use('/', baseRouter);
};

const registerUsage = function(app) {

  if (process.env.NODE_ENV !== 'production') {
    const compiler = webpack(config);

    const DevWebpackMiddleware = require('webpack-dev-middleware')(compiler, {
      noInfo: false,
      publicPath: config.output.publicPath,
      watchOptions: {
        aggregateTimeout: 300,
        poll: 1000
      }
    });

    const HotWebpackMiddleware = require('webpack-hot-middleware')(compiler);

    app.use([HotWebpackMiddleware, DevWebpackMiddleware]);
  }

  app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(express.static(path.resolve(__dirname, '..', baseHelper.getBaseDirectory())));
  app.use(function (err, req, res, next) {
    logger.error(err);
    logger.error(err.stack);
    res.status(err.status || 500).send(err.message || 'Internal server error.');
  });
};

module.exports = {
  registerRoutes,
  registerUsage
};