const User =  require("./User");
const assert = require('assert');

describe('User Entity Tests', function() {
  describe('#Initialization', function() {
    it('Init user with empty data', function() {
      const emptyUser = new User({});


      assert.equal(emptyUser.lastName, undefined);
      assert.equal(emptyUser.firstName, undefined);
      assert.equal(emptyUser.avatar, undefined);
      assert.equal(emptyUser.email, undefined);
      assert.equal(emptyUser.gender, undefined);
    });

    it('Initialization ', function() {

      const userData = {
        name: {
          first: "guy",
          last: "nimni"
        },
        picture: {
          thumbnail: "xxx"
        },
        email: "fff@fff.com",
        gender: "male"
      };

      const emptyUser = new User(userData);


      assert.equal(emptyUser.lastName, userData.name.last);
      assert.equal(emptyUser.firstName, userData.name.first);
      assert.equal(emptyUser.avatar, userData.picture.thumbnail);
      assert.equal(emptyUser.email, userData.email);
      assert.equal(emptyUser.gender, userData.gender);
    });


  });

  describe('#Methods', function() {
    it('toClientInfo', function() {
      const emptyUser = new User({});

      const clientInfo = emptyUser.toClientInfo();

      assert.equal(clientInfo.lastName, undefined);
      assert.equal(clientInfo.firstName, undefined);
      assert.equal(clientInfo.avatar, undefined);
      assert.equal(clientInfo.email, undefined);
      assert.equal(clientInfo.gender, undefined);
    });
  });
});