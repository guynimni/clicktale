class User {
  constructor (data) {
    const { name, picture } = data;

    if (name) {
      this.firstName = name.first;
      this.lastName = name.last;
    }

    if (picture) {
      this.avatar = picture.thumbnail;
    }

    this.email = data.email;
    this.gender = data.gender;
  }

  toClientInfo() {
    const {firstName, lastName, avatar, email, gender} = this;
    return {
      firstName,
      lastName,
      avatar,
      email,
      gender
    }
  }
}

module.exports = User;