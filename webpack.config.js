const path = require('path'),
      webpack = require('webpack'),
      ExtractTextPlugin = require('extract-text-webpack-plugin'),
      CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  devtool: 'inline-source-map',
  target: 'web',
  resolve: {
    alias: {
      Scripts: path.resolve(__dirname, 'client/assets/scripts'),
      Styles: path.resolve(__dirname, 'client/assets/styles'),
      Components: path.resolve(__dirname, 'client/components'),
      Pages: path.resolve(__dirname, 'client/pages'),
      Images: path.resolve(__dirname, 'client/assets/images'),
      Reducers: path.resolve(__dirname, 'client/reducers'),
      Actions: path.resolve(__dirname, 'client/actions'),
      Store: path.resolve(__dirname, 'client/store'),
      Helpers: path.resolve(__dirname, 'client/helpers'),
      API: path.resolve(__dirname, 'client/api'),
      Entities: path.resolve(__dirname, 'client/entities')
    }
  },
  entry: ['webpack-hot-middleware/client?reload=true', path.resolve(__dirname, 'client/index.js')],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'client/dist')
  },
  devServer: {
    contentBase: './client',
    host: '0.0.0.0'
  },
  context: __dirname,
  module: {
    rules: [{
      test: /\.js?$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {}
    },
    {
      test: /\.(jpe?g|png|gif|ico|svg)$/i,
      use: [
        'file-loader',
        {
          loader: 'img-loader',
          options: {
            mozjpeg: {
              progressive: true,
              arithmetic: false
            }
          }
        },
      ],
    },
    {test: /\.eot(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
    {test: /\.(woff|woff2)$/, loader: 'url-loader?prefix=font/&limit=5000'},
    {test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/, loader: 'url-loader?limit=10000&mimetype=application/octet-stream'},
    {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url-loader?limit=10000&mimetype=image/svg+xml',
      include: /node_modules/
    },
    {
      test: /(\.css|\.scss)$/,
      use: [{
        loader: 'style-loader', // inject CSS to page
      },
      {
        loader: 'css-loader', // translates CSS into CommonJS modules
      },
      {
        loader: 'sass-loader' // compiles Sass to CSS
      }]
    }]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new CopyWebpackPlugin([{ from: './client/favicon.ico' }]),
    new ExtractTextPlugin("style.css")
  ]
};