import React from 'react';
import SearchBox from 'Components/SearchBox';
import UsersAPI from 'API/UsersAPI';
import ResultsList from 'Components/UserResults';
import Loader from "Components/Loader";

class SearchPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: ""
    };
    this.onSearchChange = this.onSearchChange.bind(this);
    this.fetchData = this.fetchData.bind(this);
    this.onSearchClick = this.onSearchClick.bind(this);
  }

  componentWillUnmount() {
    if (this.fetchTimeout)
      clearTimeout(this.fetchTimeout);
  }

  onSearchChange(searchValue) {
    this.setState({searchValue});
    if (this.fetchTimeout)
      clearTimeout(this.fetchTimeout);
    this.fetchTimeout = setTimeout(this.fetchData, 2000);
  }

  onSearchClick() {
    if (this.fetchTimeout)
      clearTimeout(this.fetchTimeout);
    this.fetchData();
  }

  fetchData() {
    this.setState({loading: true});
    UsersAPI.get(this.state.searchValue).then(data => {
      this.setState({result: data.results, loading: false});
    })
  }

  renderPromoContent() {
    if (this.state.result && this.state.result.length) {
      return (
        <div>
          <img alt={this.state.result[0].name} src={this.state.result[0].avatar} />
          <span>PROMO OF {this.state.result[0].name}</span>
        </div>
      );
    }

    return null;
  }

  renderContent() {
    if (this.state.loading)
      return (<Loader/>);

    return (
      <div className={"row"}>
        <div className={"col-8 results-area"}>
          <ResultsList results={this.state.result}/>
        </div>
        <div className={"col-4 promo-area"}>
          {this.renderPromoContent()}
        </div>
      </div>
    )
  }

  render() {

    return (
        <div className={"container search-page"}>
          <div className={"row"}>
            <div className={"col-12"}>
              <SearchBox onChangeCallback={this.onSearchChange} initialValue={this.state.searchValue}/>
              <button type="button" className="btn btn-primary ml-4" onClick={this.onSearchClick}>Search</button>
            </div>
          </div>

          {this.renderContent()}
        </div>
    );
  }
}
SearchPage.propTypes = {
};

export default SearchPage;