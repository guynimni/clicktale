import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Header from 'Components/Header';
import Footer from 'Components/Footer';

class MasterPage extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div id="wrapper">
        {this.props.withNavigation && <Header/>}
        <this.props.content {...(this.props.contentProps || {})} />
        {this.props.withNavigation && <Footer/>}
      </div>
    );
  }
}

MasterPage.propTypes = {
  withNavigation: PropTypes.bool,
  content: PropTypes.func.isRequired,
  contentProps: PropTypes.object
};

export default MasterPage;