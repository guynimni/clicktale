class User {
  constructor (data) {

    this.firstName = data.firstName;
    this.lastName = data.lastName;
    this.avatar = data.avatar;
    this.email = data.email;
    this.gender = data.gender;
  }

  get name() {
    return `${this.firstName} ${this.lastName}`;
  }
}

export default User;