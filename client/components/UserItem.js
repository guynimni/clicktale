import React, { Component } from "react";
import PropTypes from "prop-types";
import User from "Entities/User";

class UserItem extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <div className={"user d-flex"}>
        <img src={this.props.user.avatar}/>
        <div className={"user-data"}>
          <div className={"user-name"}>Name: <span>{this.props.user.name}</span></div>
          <div className={"user-mail"}>Email: <span>{this.props.user.email}</span></div>
          <div className={"user-gender"}>Gender: <span>{this.props.user.gender}</span></div>
        </div>
      </div>
    );
  }
}

UserItem.propTypes = {
  user: PropTypes.instanceOf(User)
};

export default UserItem;