import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Footer extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <footer>
        <div>
          <p className="copyright" data-wow-duration="2s">© 2019 Clicktale. All Rights Reserved</p>
        </div>
      </footer>
    );
  }
}

Footer.propTypes = {};

export default Footer;