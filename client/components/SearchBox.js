import React, { Component } from "react";
import PropTypes from "prop-types";

class SearchBox extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value: props.initialValue || ""
    };

    this.updateParent = this.updateParent.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  updateParent() {
    if (this.props.onChangeCallback) {
      this.props.onChangeCallback(this.state.value);
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    }, this.updateParent);
  }

  render () {
    return (
      <div className="search-box">
        <input value={this.state.value} onChange={this.handleChange} name="value"/>
      </div>
    );
  }
}

SearchBox.propTypes = {
  initialValue: PropTypes.string,
  onChangeCallback: PropTypes.func
};

export default SearchBox;