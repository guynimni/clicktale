import React, { Component } from "react";
import PropTypes from "prop-types";
import UserItem from "Components/UserItem";

class UserResults extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <ul>
        {(this.props.results || []).map(user => <UserItem user={user} key={user.firstName + user.lastName}/>)}
      </ul>
    );
  }
}

UserResults.propTypes = {
  results: PropTypes.array
};

export default UserResults;