import React, { Component } from 'react';

class Header extends React.Component {
  constructor (props) {
    super(props);
  }

  render() {
    return (
      <nav className="header navbar navbar-expand-lg navbar-light bg-light">
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="navbar-collapse justify-content-md-center collapse" id="navbarNavAltMarkup">
          Clicktale Search People
        </div>
      </nav>
    );
  }
}

Header.propTypes = {};

export default Header;