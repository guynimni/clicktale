import React, { Component } from 'react';
import loader from 'Images/loader.svg';
import gifLoader from 'Images/loader.gif';
import PropTypes from 'prop-types';
import 'Styles/loader.scss';

class Loader extends React.Component {
  constructor (props) {
    super(props);
  }

  render () {
    return (
      <div>
        <object className="svg-block" type="image/svg+xml" data={loader}>
          <img src={gifLoader} />
        </object>
        {this.props.innerText && <h3 className={"rtl"}>{this.props.innerText}</h3>}
      </div>

    );
  }
}

Loader.propTypes = {
  innerText: PropTypes.string
};

export default Loader;