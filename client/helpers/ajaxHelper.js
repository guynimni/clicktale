class ajaxHelper {
  static fetch(requestType, url, data) {
    let requestData = {
      method: requestType,
      headers: new Headers({
        'Content-Type': 'application/json',
      })
    };

    if (requestType == ajaxHelper.requestTypes.POST && data)
      requestData.body = JSON.stringify(data);

    return fetch(url, requestData).then(data => data.json());
  }
}

ajaxHelper.requestTypes = {
  POST: "POST",
  GET: "GET"
};

export default ajaxHelper;