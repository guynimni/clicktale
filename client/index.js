import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Route, BrowserRouter, Switch, Redirect} from 'react-router-dom';
import 'Styles/base.scss';
import MasterPage from 'Pages/MasterPage';
import SearchPage from 'Pages/SearchPage';
import routes from 'Helpers/routes';

ReactDOM.render(
    <BrowserRouter>
      <Switch>
        <Route exact path={routes.index} render={function(props){
          return (<MasterPage content={SearchPage} withNavigation contentProps={{}} />);
        }} />
        <Route render={function(){return (<Redirect to="/" />);}} />
      </Switch>
    </BrowserRouter>,
  document.getElementById('react')
);

export default routes;