import AjaxUtils from 'Helpers/ajaxHelper';
import User from 'Entities/User';

class UsersAPI {
  static get(phrase) {
    let URL = '/users';

    if (phrase)
      URL += `?query=${phrase}`;

    return AjaxUtils.fetch(AjaxUtils.requestTypes.GET, URL).then(data => {
      data.results = data.results.map(userData => new User(userData));
      return data;
    });
  }
}

export default UsersAPI;